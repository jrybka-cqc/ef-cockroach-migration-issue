using ef_cockroach_migration_issue.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ef_cockroach_migration_issue.Infrastructure.Persistence.Configurations;

public class BookAuthorConfiguration : IEntityTypeConfiguration<BookAuthor>
{
    public void Configure(EntityTypeBuilder<BookAuthor> builder)
    {
        builder.ToTable(nameof(BookAuthor));

        builder.HasOne(d => d.Book)
            .WithMany(p => p.BookAuthors)
            .HasForeignKey(d => d.BookId)
            .OnDelete(DeleteBehavior.Restrict);
        
        builder.HasOne(d => d.Author)
            .WithMany(p => p.BookAuthors)
            .HasForeignKey(d => d.AuthorId)
            .OnDelete(DeleteBehavior.Restrict);
    }
}