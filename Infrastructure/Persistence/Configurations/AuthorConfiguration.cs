using ef_cockroach_migration_issue.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ef_cockroach_migration_issue.Infrastructure.Persistence.Configurations;

public class AuthorConfiguration : IEntityTypeConfiguration<Author>
{
    public void Configure(EntityTypeBuilder<Author> builder)
    {
        builder.ToTable(nameof(Author));

        builder.Property(e => e.GivenName).HasMaxLength(255);
        builder.Property(e => e.FamilyName).HasMaxLength(255);
    }
}