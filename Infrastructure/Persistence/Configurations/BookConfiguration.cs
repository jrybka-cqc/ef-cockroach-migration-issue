using ef_cockroach_migration_issue.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ef_cockroach_migration_issue.Infrastructure.Persistence.Configurations;

public class BookConfiguration : IEntityTypeConfiguration<Book>
{
    public void Configure(EntityTypeBuilder<Book> builder)
    {
        builder.ToTable(nameof(Book));

        builder.Property(e => e.Title).HasMaxLength(1024);

        builder.HasMany(p => p.Authors)
            .WithMany(p => p.Books)
            .UsingEntity<BookAuthor>();
    }
}