START TRANSACTION;

ALTER TABLE "Book" DROP CONSTRAINT "FK_Book_Author_AuthorId";

DROP INDEX "IX_Book_AuthorId";

ALTER TABLE "Book" DROP COLUMN "AuthorId";

CREATE TABLE "BookAuthor" (
    "Id" uuid NOT NULL,
    "BookId" uuid NOT NULL,
    "AuthorId" uuid NOT NULL,
    CONSTRAINT "PK_BookAuthor" PRIMARY KEY ("Id"),
    CONSTRAINT "FK_BookAuthor_Author_AuthorId" FOREIGN KEY ("AuthorId") REFERENCES "Author" ("Id") ON DELETE RESTRICT,
    CONSTRAINT "FK_BookAuthor_Book_BookId" FOREIGN KEY ("BookId") REFERENCES "Book" ("Id") ON DELETE RESTRICT
);

CREATE INDEX "IX_BookAuthor_AuthorId" ON "BookAuthor" ("AuthorId");

CREATE INDEX "IX_BookAuthor_BookId" ON "BookAuthor" ("BookId");

INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20230127211718_MultiAuthorChange', '6.0.13');

COMMIT;


