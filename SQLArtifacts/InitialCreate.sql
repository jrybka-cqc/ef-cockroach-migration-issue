CREATE TABLE IF NOT EXISTS "__EFMigrationsHistory" (
    "MigrationId" character varying(150) NOT NULL,
    "ProductVersion" character varying(32) NOT NULL,
    CONSTRAINT "PK___EFMigrationsHistory" PRIMARY KEY ("MigrationId")
);

START TRANSACTION;

CREATE TABLE "Author" (
    "Id" uuid NOT NULL,
    "GivenName" character varying(255) NOT NULL,
    "FamilyName" character varying(255) NOT NULL,
    CONSTRAINT "PK_Author" PRIMARY KEY ("Id")
);

CREATE TABLE "Book" (
    "Id" uuid NOT NULL,
    "AuthorId" uuid NOT NULL,
    "Title" character varying(1024) NOT NULL,
    CONSTRAINT "PK_Book" PRIMARY KEY ("Id"),
    CONSTRAINT "FK_Book_Author_AuthorId" FOREIGN KEY ("AuthorId") REFERENCES "Author" ("Id") ON DELETE RESTRICT
);

CREATE INDEX "IX_Book_AuthorId" ON "Book" ("AuthorId");

INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20230127210923_InitialCreate', '6.0.13');

COMMIT;


