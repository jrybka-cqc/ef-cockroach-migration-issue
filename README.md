This project requires the use of the .NET6 SDK.

To reproduce the issues with this repository, you must either edit appsettings.Development.json or create a developer override
appsettings (detailed below) with the default connection string defined as required by Npgsql.  You must also have 
the entity framework CLI tools installed in your environment:

https://learn.microsoft.com/en-us/ef/core/cli/dotnet

Run the following:

```shell
DEVELOPER_SETTINGS_OVERRIDE=<name> dotnet ef database update
```

#### Developer Settings Override Example (pasted from internal documentation)
In order to accomodate different developer-specific settings without polluting our repository, you can create a
developer-specific appsettings.{name}.json file in your local repository.  You should be able to set only the
settings that you want to override from the Development appsettings file.  To instruct the system to add your
developer-specific file at start up, you should set the environment variable `DEVELOPER_SETTINGS_OVERRIDE` to the
value you used for `{name}` above.  This would look similar to the following, if your appsettings file was named
`appsettings.mine.json` with the following content:

```json
{
  "ConnectionStrings": {
    "DefaultConnection": "Host=host;Port=12354;Database=db;Options=--opts"
  }
}
```

You would be able to run the following to have that specific setting overridden:

```shell
DEVELOPER_SETTINGS_OVERRIDE=mine ASPNETCORE_ENVIRONMENT=Development dotnet run
```

You must be running in development mode for this configuration to apply.  This is implemented in `Disco/src/RestApi/Program.cs`.
All files other than appsettings.json, appsettings.Development.json, and appsettings.Production.json are ignored
through the .gitignore file.