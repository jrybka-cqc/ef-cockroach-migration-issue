namespace ef_cockroach_migration_issue.Domain;

public class Author
{
    public Author()
    {
        Books = new HashSet<Book>();
        BookAuthors = new HashSet<BookAuthor>();
    }
    
    public Guid Id { get; set; }
    public string GivenName { get; set; } = null!;
    public string FamilyName { get; set; } = null!;
    
    public ICollection<Book> Books { get; set; }
    public ICollection<BookAuthor> BookAuthors { get; set; }
}