namespace ef_cockroach_migration_issue.Domain;

public class Book
{
    public Book()
    {
        Authors = new HashSet<Author>();
        BookAuthors = new HashSet<BookAuthor>();
    }
    
    public Guid Id { get; set; }
    public string Title { get; set; } = null!;
    
    public ICollection<Author> Authors { get; set; }
    public ICollection<BookAuthor> BookAuthors { get; set; }
}