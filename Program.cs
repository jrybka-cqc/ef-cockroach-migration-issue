using ef_cockroach_migration_issue;

var builder = WebApplication.CreateBuilder(args);

if (builder.Environment.IsDevelopment() 
    && Environment.GetEnvironmentVariable("DEVELOPER_SETTINGS_OVERRIDE") is { } devOverride)
{
    builder.Configuration.AddJsonFile($"appsettings.{devOverride}.json", optional: true, reloadOnChange: true);
}

// Add services to the container.
builder.Services.AddAppServices(builder.Configuration);

var app = builder.Build();

app.AddAppPipeline();

app.Run();