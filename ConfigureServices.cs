using ef_cockroach_migration_issue.Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using Npgsql;

namespace ef_cockroach_migration_issue;

public static class ConfigureServices
{
    public static IServiceCollection AddAppServices(this IServiceCollection services, IConfiguration configuration)
    {
        var connectionStringBuilder = new NpgsqlConnectionStringBuilder(configuration.GetConnectionString("DefaultConnection"));
        
        services.AddDbContext<ApplicationDbContext>(options =>
        {
            options
                .UseNpgsql(connectionStringBuilder.ConnectionString,
                    o => o.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName));
        });

        services.AddControllers();

        return services;
    }

    public static WebApplication AddAppPipeline(this WebApplication app)
    {
        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        
        app.MapControllers();
        return app;
    }
}